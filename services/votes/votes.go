package votes

import (
	"github.com/Shopify/sarama"
	"github.com/pkg/errors"
	votingpb "gitlab.com/block-chain-voting/proto"
	"google.golang.org/protobuf/proto"
)

type Service struct {
	prod           sarama.SyncProducer
	validatorTopic string
}

func NewService(prod sarama.SyncProducer, validatorTopic string) *Service {
	return &Service{prod: prod, validatorTopic: validatorTopic}
}

func (s *Service) RequestValidation(campaignID, candidateID int64, passport, fullname string) error {
	vote := &votingpb.Vote{
		Voter: &votingpb.Voter{
			Passport: passport,
			Fullname: fullname,
		},
		CampaignId:  campaignID,
		CandidateId: candidateID,
		Status:      votingpb.VoteStatus_OK,
	}

	b, err := proto.Marshal(vote)
	if err != nil {
		return errors.Wrapf(err, "failed to marshal vote %#v", vote)
	}

	_, _, err = s.prod.SendMessage(&sarama.ProducerMessage{
		Topic: s.validatorTopic,
		Value: sarama.ByteEncoder(b),
	})
	return errors.Wrapf(err, "failed to send %#v to topic=%s", vote, s.validatorTopic)
}
