package votes

import (
	"gitlab.com/block-chain-voting/parser/apierr"
)

var ErrInvalidInput = &apierr.Error{
	Code:    1,
	Message: "invalid input data",
}
