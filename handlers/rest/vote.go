package rest

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/block-chain-voting/parser/handlers/rest/requests"
	"gitlab.com/block-chain-voting/parser/services/votes"
)

type voteCtrl struct {
	votes *votes.Service
}

func newVoteCtrl(votes *votes.Service) *voteCtrl {
	return &voteCtrl{votes: votes}
}

func (vc *voteCtrl) RegisterVote(ctx *gin.Context) {
	var req requests.Vote
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.Error(errors.Wrap(votes.ErrInvalidInput, err.Error()))
		return
	}

	if err := vc.votes.RequestValidation(req.CampaignID, req.CandidateID, req.Voter.Passport, req.Voter.Fullname); err != nil {
		ctx.Error(err)
	}
}
