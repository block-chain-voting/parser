package requests

type Vote struct {
	CampaignID  int64 `json:"campaign_id" binding:"required"`
	CandidateID int64 `json:"candidate_id" binding:"required"`
	Voter       struct {
		Passport string `json:"passport" binding:"required"`
		Fullname string `json:"fullname" binding:"required"`
	} `json:"voter"`
}
