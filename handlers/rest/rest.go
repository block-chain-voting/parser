package rest

import (
	"net/http"

	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"

	"gitlab.com/block-chain-voting/parser/services/votes"
)

func NewHandler(votes *votes.Service) http.Handler {
	mux := gin.Default()
	mux.Use(RequestID(), ErrorHandler())

	vc := newVoteCtrl(votes)
	mux.POST("/vote", vc.RegisterVote)

	return mux
}

func NewHealthHandler(version string, saramaClient sarama.Client) (http.Handler, error) {
	mux := gin.Default()

	hc, err := newHealthCtrl(version, saramaClient)
	if err != nil {
		return nil, err
	}
	mux.GET("/ready", hc.ready)

	return mux, nil
}
