package rest

import (
	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

const healthTopic = "health"

type healthCtrl struct {
	version string
	admin   sarama.ClusterAdmin
	prod    sarama.SyncProducer
}

func newHealthCtrl(version string, saramaClient sarama.Client) (*healthCtrl, error) {
	admin, err := sarama.NewClusterAdminFromClient(saramaClient)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init sarama admin")
	}

	prod, err := sarama.NewSyncProducerFromClient(saramaClient)
	if err != nil {
		return nil, errors.Wrap(err, "failed to init sarama sync producer")
	}

	return &healthCtrl{
		version: version,
		admin:   admin,
		prod:    prod,
	}, nil
}

func (hc *healthCtrl) ready(ctx *gin.Context) {
	var isErr bool
	res := struct {
		Version string `json:"version"`
		Kafka   string `json:"kafka"`
	}{
		Version: hc.version,
		Kafka:   "ok",
	}

	if err := hc.checkKafka(); err != nil {
		res.Kafka = err.Error()
		isErr = true
	}

	if isErr {
		ctx.AbortWithStatusJSON(503, res)
		return
	}
	ctx.JSON(200, res)
}

func (hc *healthCtrl) checkKafka() error {
	if err := hc.admin.CreateTopic(healthTopic, &sarama.TopicDetail{
		NumPartitions:     1,
		ReplicationFactor: 1,
	}, false); err != nil {
		return err
	}

	if _, _, err := hc.prod.SendMessage(&sarama.ProducerMessage{Topic: healthTopic}); err != nil {
		return err
	}

	return hc.admin.DeleteTopic(healthTopic)
}
