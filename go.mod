module gitlab.com/block-chain-voting/parser

go 1.16

require (
	github.com/Shopify/sarama v1.29.0
	github.com/gin-gonic/gin v1.7.4
	github.com/google/uuid v1.3.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.22.0
	gitlab.com/block-chain-voting/proto v0.3.6
	google.golang.org/protobuf v1.26.0
)
