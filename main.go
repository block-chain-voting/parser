package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/Shopify/sarama"
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"

	"gitlab.com/block-chain-voting/parser/handlers/rest"
	"gitlab.com/block-chain-voting/parser/services/votes"
)

var (
	version string
	envs    struct {
		Web struct {
			Addr            string        `envconfig:"WEB_ADDR" default:":3000"`
			HealthAddr      string        `envconfig:"WEB_HEALTH_ADDR" default:":4000"`
			ShutdownTimeout time.Duration `envconfig:"WEB_SHUTDOWN_TIMEOUT" default:"5s"`
		}
		Kafka struct {
			Addr           string `envconfig:"KAFKA_ADDR" required:"true"`
			ValidatorTopic string `envconfig:"KAFKA_VALIDATOR_TOPIC" required:"true"`
		}
	}
)

func main() {
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	log.Logger = zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr}).With().Caller().Logger()

	if err := run(); err != nil {
		log.Fatal().Stack().Err(err).Msg("Failed to start service")
	}
}

func run() error {
	if err := godotenv.Load(); err != nil && !os.IsNotExist(err) {
		log.Warn().Err(err).Msg("Read ENVs from .env file")
	}
	if err := envconfig.Process("", &envs); err != nil {
		return errors.Wrap(err, "failed to parse ENVs to struct")
	}

	log.Info().Interface("envs", envs).Msg("ENVs")

	cfg := sarama.NewConfig()
	cfg.Producer.Return.Errors = true
	cfg.Producer.Return.Successes = true

	saramaClient, err := sarama.NewClient([]string{envs.Kafka.Addr}, cfg)
	if err != nil {
		return errors.Wrapf(err, "failed to connect to Kafka %q", envs.Kafka.Addr)
	}

	prod, err := sarama.NewSyncProducerFromClient(saramaClient)
	if err != nil {
		return errors.Wrapf(err, "failed to start producer on %q", envs.Kafka.Addr)
	}

	defer func() {
		if err := prod.Close(); err != nil {
			log.Error().Stack().Err(err).Msg("Failed to close sarama producer")
		}
	}()

	healthHandler, err := rest.NewHealthHandler(version, saramaClient)
	if err != nil {
		return err
	}

	httpErrs := make(chan error)
	go func() {
		log.Info().Msgf("Health server is listening on %s...", envs.Web.HealthAddr)
		if err := http.ListenAndServe(envs.Web.HealthAddr, healthHandler); err != nil &&
			!errors.Is(err, http.ErrServerClosed) {
			httpErrs <- err
		}
	}()

	srv := http.Server{
		Addr:    envs.Web.Addr,
		Handler: rest.NewHandler(votes.NewService(prod, envs.Kafka.ValidatorTopic)),
	}

	go func() {
		log.Info().Msgf("Parser REST service is listening on %q", envs.Web.Addr)
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			httpErrs <- err
		}
	}()

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT)

	select {
	case err := <-httpErrs:
		return err
	case sig := <-shutdown:
		ctx, cancel := context.WithTimeout(context.Background(), envs.Web.ShutdownTimeout)
		defer cancel()

		log.Info().Msg("Shutdown server...")
		if err := srv.Shutdown(ctx); err != nil {
			return errors.Wrapf(err, "failed to shutdown server")
		}
		log.Info().Msgf("Terminated via signal %q", sig)
	}

	return nil
}
